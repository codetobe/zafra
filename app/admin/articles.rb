ActiveAdmin.register Article do
  menu :label => "3. Articulos"

  form do |f|
    f.inputs "A&ntilde;ade un art&icute;culo" do
      f.input :title
      f.input :description, as: :wysihtml5, blocks: [ :h3, :p, :strong]
    end

    f.buttons
  end
end
