ActiveAdmin.register Contact do
  menu :label => "6. Contacta"
  
  form do |f|
    f.inputs "A&ntilde;ade un texto a la pantalla de contacto" do
      f.input :title
      f.input :description, as: :wysihtml5, blocks: [ :h3, :p, :strong]
    end

    f.buttons
  end  
end
