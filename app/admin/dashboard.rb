ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Personaliza tu Portada" do
          ul do
            Home.find(1) do |home|
              li link_to(home.title, admin_home_path(home))
            end
          end
        end
      end

      column do
        panel "Personaliza tus Servicios" do
          ul do
            Service.all.each do |service|
              li link_to(service.title, admin_service_path(service))
            end
          end
        end
      end
    end
    
    columns do
      column do
        panel "Personaliza el Contacta" do
          ul do
            Contact.all.each do |contact|
              li link_to(contact.title, admin_contact_path(contact))
            end
          end
        end
      end

      column do
        panel "Personaliza tus Imagenes" do
          ul do
            Image.all.each do |image|
              li link_to(image.title, admin_image_path(image))
            end
          end
        end
      end

    end
  end # content
end
