ActiveAdmin.register Home do
  menu :label => "1. Texto portada"

  form do |f|
    f.inputs "A&ntilde;ade un texto a la pantalla de inicio" do
      f.input :title
      f.input :description, as: :wysihtml5, blocks: [ :h3, :p, :strong]
      f.input :image, as: :file
    end

    f.buttons
  end
end
