ActiveAdmin.register Service do
  menu :label => "2. Servicios"

  form do |f|
    f.inputs "A&ntilde;ade un servicio" do
      f.input :title
      f.input :description, as: :wysihtml5, blocks: [ :h3, :p, :strong]
      f.input :image, as: :file
    end

    f.buttons
  end
end
