# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->
	$('#consulta').submit ->
		submit = $('input[type="submit"]')
		submit.attr 'disabled', 'true' # disable button on ajax call
		submit.attr 'value', 'Enviando consulta' # set other value on button
		
		# make ajax call
		
		$.ajax
			data: $('#consulta').serialize()
			url: '/consulta.json'
			success: (data) ->
				notice = $('#notice')
				
				# have error
				
				if data.status is 'error'
					# insert all messages of errors
					$.each data.message, (input) ->
						$('#error-' + input).html data.message[input][0]
						$('#error-' + input).addClass 'visible'
						
					# show and hide general message error
					notice.fadeIn().delay(2000).fadeOut()
						
					# reactivate submit button
					submit.removeAttr 'disabled'
					submit.attr 'value', 'Enviar consulta'
					
				# not error
				
				else
					notice.remove
					submit.attr 'value', data.message
					submit.attr 'disabled', 'true'
					submit.addClass 'ok'
					
		false # return false on after in submit
		
	$('input[type="text"], input[type="email"], textarea').focus ->
		$('#error-' + $(this).attr('id')).removeClass('visible')