class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :meta_defaults
  private

  def meta_defaults
    @meta_title = ""
    @meta_keywords = ""
    @meta_description = ""
  end
end
