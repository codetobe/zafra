class ArticlesController < ApplicationController
  def index
    @articles = Article.all
  end

  def show
    id = params[:id]
    @article = Article.find(id)
    render :show
  end

  def meta_defaults
    @meta_title = "Articulos"
    @meta_keywords = "zafra, peluqueria, perros, gatos, animales, veterinario, veterinaria, contacta, formulario, madrid"
    @meta_description = "Art&iacute;culo de Zafra cl&iacute;nica veterinaria."
  end

end
