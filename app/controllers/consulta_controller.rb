class ConsultaController < ApplicationController
  
  def enviar    
    if params[:nombre] then
      @contacta = Consulta.new
      @contacta.nombre = params[:nombre]
      @contacta.mail = params[:mail]
      @contacta.texto = params[:texto]
      
      if @contacta.save then
        UserMailer.contacta_email(@contacta).deliver # send mail 
        feedback = {
          :status => 'ok',
          :message => 'Tu consulta ha sido enviada correctamente'
        }
      else
        feedback = {
          :status => 'error',
          :message => @contacta.errors #return all the error to javascript
        }
      end
      
      render :json => feedback
    end
  end
end