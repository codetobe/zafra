class ContactsController < ApplicationController
  # GET /contacts
  # GET /contacts.json
  def index
    @contact = Contact.first
  end
  
  def meta_defaults
    @meta_title = "Contacta "
    @meta_keywords = "zafra, peluqueria, perros, gatos, animales, veterinario, veterinaria, contacta, formulario, madrid"
    @meta_description = "Ponte en contacto con la clinica para cualquier consulta."
  end
    
end
