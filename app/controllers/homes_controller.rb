class HomesController < ApplicationController

  def index
    @homes = Home.first
  end

  def meta_defaults
    @meta_title = "Inicio"
    @meta_keywords = "zafra, peluqueria, perros, gatos, animales, veterinario, veterinaria, contacta, formulario, madrid"
    @meta_description = "Cl&iacute;nica Veterinaria Zafra lleva muchos a&ntilde;os dedic&aacute;ndose al cuidado de su mascota, con el fin de satisfacer sus necesidades. Daniel Saiz, se encarg&oacute; de llevar a cabo este gran proyecto en P&ordm; Marqu&eacute;s de Zafra n&ordm; 35"
  end

end
