class ProductsController < ApplicationController
  def index
    @products = Product.all
  end

  def categoria
    id = params[:id]
    @products = Product.where("category_id = #{id}")
    render :index
  end

  def meta_defaults
    @meta_title = "Productos "
    @meta_keywords = "zafra, peluqueria, perros, gatos, animales, veterinario, veterinaria, contacta, formulario, madrid"
    @meta_description = "Estos son los productos que puedes encontrar en Zafra cl&iacute;nica veterinaria"
  end

end
