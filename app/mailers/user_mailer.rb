class UserMailer < ActionMailer::Base
  default from: "mail@zafraveterinario.com"

  def contacta_email(contacta)
    @contacta = contacta
    mail :to => "ricardo.saiz@gmail.com",
         :from => "mail@zafraveterinario.com", 
         :subject => "[zafra] Consulta desde la web"
  end
end
