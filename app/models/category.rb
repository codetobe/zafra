class Category < ActiveRecord::Base
  attr_accessible :description, :parent_id, :title
  belongs_to :category, class_name: "Category", foreign_key: "parent_id"
  has_many :products

  def get_categories
    @categories = Category.all
    @categories.each do |category| 
      if have_subcategories?(category.id)
        category['subcategories'] = false
      else 
        category['subcategories'] = true
      end
    end
  end

  def have_subcategories?(id)
    @categories.each do |category|
      if category.parent_id == id
        return false
      end
    end
    return true
  end
end
