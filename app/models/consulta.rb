class Consulta < ActiveRecord::Base
  attr_accessible :mail, :nombre, :texto
  
  validates :nombre, :presence => { :message => "Campo obligatorio" }
  validates :texto, :presence => { :message => "Campo obligatorio" }
  validates :mail, :presence => { :message => "Campo obligatorio" }
  validates :mail, :format => {
    :with => /^([^\s]+)((?:[-a-z0-9]\.)[a-z]{2,})$/i,
    :message => 'Revisa el e-mail'
  }
  
end