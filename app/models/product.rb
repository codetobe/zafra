class Product < ActiveRecord::Base
  attr_accessible :parent_id, :description, :image, :price, :title, :category_id
  belongs_to :category, class_name: "Category", foreign_key: "category_id"
  mount_uploader :image, ImageUploader
end