# encoding: utf-8

set :user, "root"
set :use_sudo, false
set :project, "zafra"
set :application, "zafra"

ip = '109.74.201.90'

beta_server = ip
master_server = ip

default_run_options[:pty] = true
set :repository, "git@bitbucket.org:codetobe/zafra.git"
set :local_repository, "#{File.dirname(__FILE__)}/../"
set :scm, :git
set :deploy_via, :remote_cache

#set :use_sudo, false
#set :chmod755, "app config lib public"

ssh_options[:forward_agent] = true

   task :beta do
      set :branch, "beta"
      role :web, beta_server, :primary => true
      set :applicationdir, "/sites/beta_zafra/"
      set :deploy_to, applicationdir
   end

   task :master do
      set :branch, "master"
      role :web, master_server, :primary => true
      set :applicationdir, "/sites/zafra/"
      set :deploy_to, applicationdir
   end
 
namespace :deploy do

after "deploy:update_code" do
   # run "rm /tmp/mysql.sock"
   # run "ln -s /run/mysqld/mysqld.sock /tmp/mysql.sock"
   # run "cd /sites/zafra/current && rake assets:clean RAILS_ENV=production && rake assets:precompile RAILS_ENV=production"
   cleanup
end

 #task :migrate do
   # do nothing
 #end

 task :restart do
  run "service nginx reload"
 end
end