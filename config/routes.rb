Zafra::Application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config

  ActiveAdmin.routes(self)

  resources :services
  resources :products
  resources :images
  resources :homes
  resources :contacts
  resources :categories
  resources :articles

  match 'servicios' => 'services#index'
  match 'contacta' => 'contacts#index'
  match 'consulta' => 'consulta#enviar'
  match 'productos' => 'products#index'
  match 'articulos' => 'articles#index'
  match 'articulo/:id' => 'articles#show'
  match 'categoria/:id' => 'products#categoria'
  match 'galeria' => 'images#index'

  root :to => 'homes#index'
end
